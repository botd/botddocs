#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup

def readme():
    with open('README.rst') as file:
        return file.read()

setup(
    name='botddocs',
    version='1',
    url='https://bitbucket.org/botd/botddocs',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description="BOTD - IRC channel daemon. no copyright. no LICENSE.",
    long_description=readme(),
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=True,
    install_requires=["botd"],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
